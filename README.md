# Developer Containerized Workspace (DevCon1)

## What is DevCon1?

It is a play on words for [DefCon](https://en.wikipedia.org/wiki/DEFCON) 1 used in the U.S. for military alerts.

This project though is to solve a problem that I have had developing software for many years.
* I don't want to use my local OS to develop software because I can't afford the down time from my communication tools (Outlook, Browsers, etc.) when my development corrupts the local OS.
* Virtual Machines running on the local OS were sufficient for a while for developing.  They provided snapshots, multiple OS's, and configurations for different software projects.  Over time VMs also became a lot of work.  Managing VMs required time in updating the OS's, updating the Virtual Machine software (hypervisor), and generally managing the infrastructure of the VM's even locally on the system.

At the end of the day, I was spending more time managing my infrastructure to develop software than I was developing the software.  I want to be able to have a development environment that I can startup quickly, have all of the software tools already updated and installed, and be ready for me to start developing the project without managing the infrastructure.

DevCon1 is intended to provide this solution in a container package.

## MVP Release

The first development environment I need is to update Indigon Inc. website with new content.  I have chosen to use the [Vuetify](https://vuetifyjs.com/en/) as my client side framework to take advantage of the community support and all of the components that are supported in making a visually appealing website.  This also means that I will need to install Node.js to build and package the website.

The website is not the focus of DevCon1, providing a fast development environment in order to develop the website is the focus.

## How to use

## Setup AWS

* If you don't already have an AWS account, [create and activate a new AWS account](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)
  * To keep things inexpensive, select the free tier as much as you can for support and other services.

**Note**
  * The below [blueprints](https://docs.tuono.com/userguide/cloud-provisioning/blueprints) that deploy the virtual machines (VM) will be set to the Oregon (us-west-2) datacenter, in the AWS console make sure you select that datacenter to see your resources.

## Create your own Forked Project

* Fork the devcon1_vuetify project so that you can control the CI and other files.
  * **NOTE :** This document assumes you are using the same "devcon1_vuetify" name, if you rename your project please replace the name with yours.
* You will then need to rebuild the docker image for your Forked Project, this may have already run due to the ".gitlab-ci.yml" configuration.  If it did not, change the Dockerfile with a small comment and check it in to trigger the docker image rebuild.
* Then update the last 2 lines in tuono/decon1_blueprint.yml with your forked project name to run your docker images.
  ```
    - docker login -u (( registry_user )) -p (( registry_password )) registry.gitlab.com/<Your Project>
    - docker run -td -p 4040:22 --name devcon1_vuetify registry.gitlab.com/devcon2/<Your Project>
  ```

## Setup GitLab Access Token

* To create your Access Tokens in GitLab go to your User Settings in the upper right corner.
* Select ["Access Tokens"](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) section.
* Enter a name for your personal access token
* Select an Expiration if you choose to have these expire for better security.
* Select the Scope as "API"
* Select the "Create personal access token" button.
* **Important** Make sure to save the personal access token in a secure location because this is the ONLY time you will see it.
  * If you lose this token you can revoke the token and create a new one with the steps above.

## Setup Tuono

* Go to [Tuono Community Edition account](https://www.tuono.com/community-edition/) and create an account.
  * If the Tutorial shows up, follow the steps to learn how Tuono works, this will help with later steps.
* Copy the devcon1_vuetify/tuono/decon1_blueprint.yml content into a new "DevCon1" blueprint on [Tuono's web console](https://docs.tuono.com/userguide/getting-started/concepts/navigating-the-portal#the-blueprints-page).
* Setup the AWS credentials under the ["Credentials"](https://docs.tuono.com/userguide/getting-started/concepts/navigating-the-portal#the-credentials-page) tab on the left.
  * See Tuono's [help guide](https://docs.tuono.com/userguide/account-management/organizations/venue-credentials) for more details.
* Setup an [Environment](https://docs.tuono.com/userguide/getting-started/concepts/navigating-the-portal#the-environments-page) called "DevCon1"
* Add the "DevCon1" blueprint to the Environment.
* Update the admin_public_key with your [SSH public key](https://docs.tuono.com/userguide/getting-started/tutorials/ssh-keypairs).
* Update the registry_user with your GitLab username, and the registry_password, with the GitLab Personal Access Token, created above.

## Setup GitLab CI variables.

* In GitLab go to Settings, CI / CD section.
* Expand the Variables section
* Add the following [variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui):
  * TUONO_USER : this is your TUONO username.
  * TUONO_PWD : this is your TUONO password.
 
## Run Pipeline
* [Run a Gitlab CI / CD pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually)

### Setting up Remote VSCode

* Install the `Remote Development (ms-vscode-remote.vscode-remote-extensionpack)` in your VScode instance.
* Select "Connect to Remote Host" by selecting the left hand icon for SSH Targets, add a new host.
* Each time you connect to the new container runtime VSCode will ask you for the Platform Type: (Linux, Windows, MacOS).  If you would like to manually set this once then you can go to the settings.json file at `/C:/Users/<username>/AppData/Roaming/Code/User/settings.json` and add the following json:
```
{
    ...
    "remote.SSH.remotePlatform": {
        "devcon_AWS_VM": "linux"
    },
    ...
}
```
  * If you use the SSH Config above with the host set to an arbitrary name, then this will work even when the IP address changes each time.

### Setting up SSH

* Setup the the `C:\Users\<username>\.ssh\config`
```
Host devcon_AWS_VM
  Port 4040
  HostName <AWS Public IP Address>
  User devcon
  IdentityFile ~/.ssh/id_rsa
```
* Update the AWS Public Address
* Login using `ssh -p 4040 devcon@<AWS Public IP>`
* Connect using VSCode and provide the password `devcon`
* You can now clone the project you want.

#### Setting up SSH Authorized_Keys (Optional)
* Create the public and private keys
  * See [SSH Keygen](https://www.ssh.com/ssh/keygen/)
* Copy the public key to the container image: `ssh-copy-id -i ~/.ssh/devcon.pub devcon@<AWS Public IP>`
* Alternatively, copy the public key into the `/home/devcon/.ssh/authorized_keys`.
```
cat .ssh/id_rsa.pub > .ssh/authorized_keys; scp -P 4040 .ssh/authorized_keys devcon@<AWS Public IP>:.ssh/
```

## Destroy your environment!!!

* Don't forget to run the destroy_tuono_environment when you are done to clean up resources and avoid getting charged!

## Acknowledgements

* I would like to thank the [Tuono Team](https://www.tuono.com/about/) for all of their help and support during the development of this project.
  * Darryl Diosomito <darryl.diosomito@tuono.com>
  * Scott Harrison <scott.harrison@tuono.com>
  * Andre Yanni <andre.yanni@tuono.com>
  * Jesse StLaurent <jesse.stlaurent@tuono.com>