#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2021 Tuono, Inc
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
'''
Tuono REST module
'''

import logging
import json
import sys
import requests

logger = logging.getLogger(__name__)


class Tuono():
    '''Base class'''

    def __init__(self, username, password):
        '''Instantiate a generic connection instance'''

        self.base_uri = 'https://portal.tuono.io/api/v1'

        self.headers = {'Content-Type': 'application/json'}

        self.creds = {'username': username,
                      'password': password}

        self.get_token()

    def get_token(self):

        request = requests.post(f'{self.base_uri}/auth/login',
                                headers=self.headers,
                                data=json.dumps(self.creds))
        if not request.ok:
            logger.error(f"ERror getting auth token: {request.text}")
            sys.exit(1)

        self.token = f'Bearer {request.json()["object"]["token"]}'

        self.headers = {'Authorization': self.token,
                        'Content-Type' : 'application/json'}

    def operation(self, verb, url, retry_count=None, **kwargs):
        if retry_count is None:
            retry_count = 2
        headers = kwargs.pop('headers', self.headers)
        res = requests.request(verb, url, headers=headers, **kwargs)
        if res.status_code in (401, 403, 409):
            logger.debug(f"Auth Error: {res.status_code}")
            if retry_count < 1:
                logger.debug("Too many retries on re-auth")
                sys.exit(1)
            self.get_token()
            return self.operation(verb, url, retry_count=retry_count - 1,
                                  headers=self.headers, **kwargs)
        return res

    def get_blueprints(self):
        '''Invoke Blueprint GET method'''

        request = self.operation('get', f'{self.base_uri}/blueprints')

        return request.json()['objects']

    def get_environments(self):
        '''Invoke Environment GET method'''

        request = self.operation("get", f'{self.base_uri}/environments',
                                 headers=self.headers)

        return request.json()['objects']

    def get_job(self, job_id):
        '''Invoke Job GET method'''

        request = self.operation('get', f'{self.base_uri}/job/{job_id}')
        return request.json()['object']

    def get_jobs(self):
        '''Invoke Job GET method'''

        request = self.operation("get", f'{self.base_uri}/jobs',
                                 headers=self.headers)

        return request.json()['objects']

    def get_organizations(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/organizations',
                                 headers=self.headers)

        return request.json()['objects']

    def get_roles(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/roles',
                                 headers=self.headers)

        return json.loads(request.json())

    def get_secrets(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/secrets',
                                 headers=self.headers)

        return request.json()['objects']

    def retrieve_secret_value(self, payload):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/secret',
                                 headers=self.headers,
                                 params=payload)

        return request.json()['object']

    def get_sessions(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/sessions',
                                 headers=self.headers)

        return request.json()['object']

    def get_users(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/users',
                                 headers=self.headers)

        return request.json()['objects']

    def get_credentials(self):
        '''Invoke Organization GET method'''

        request = self.operation("get", f'{self.base_uri}/credentials',
                                 headers=self.headers)

        return request.json()['objects']

    def complete_action(self, payload):
        '''Invoke complete action POST method'''

        request = self.operation("post", f'{self.base_uri}/job',
                                 headers=self.headers,
                                 data=json.dumps(payload))

        return request.json()['object']

    def create_environment(self, payload):
        '''Invoke create Environment POST method'''

        request = self.operation("post", f'{self.base_uri}/environment',
                                 headers=self.headers,
                                 data=json.dumps(payload))

        return request.json()['object']

    def set_variable(self, payload, environment_id):
        '''Invoke set Environment Variable PUT method'''

        request = self.operation("put", f"{self.base_uri}/environment/ "
                                        f"{environment_id}/variable",
                                 headers=self.headers,
                                 data=json.dumps(payload))

        return request.json()['object']

    def delete_environment(self, environment_id):
        '''Invoke delete Environment DELETE method'''

        request = self.operation("delete",f"{self.base_uri}/environment/ "
                                          f"{environment_id}",
                                  headers=self.headers)

        return request.status_code

    def add_credentials(self, payload):
        '''Invoke set Environment Variable PUT method'''

        request = self.operation("post", f"{self.base_uri} "
                                         f"/vault_insert_credential",
                                 headers=self.headers,
                                 data=json.dumps(payload))

        return request.json()['object']
