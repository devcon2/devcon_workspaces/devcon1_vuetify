#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2021 Tuono, Inc
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
'''
Basic Tuono task execution Client.

USAGE: ./api_get.py --username <username>
                    --password <password>
                    --environment '<environment_friendly_name>'
                    --action 'apply'|'destroy'|'simulate'
'''

import argparse
import datetime
import getpass
import json
import sys
import time
import requests
import tuono_api

# Disable SSL warnings
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def colours(text, colour=None):
    ''' Colourise text '''

    if colour == 'bold':
        colour = '1m'
    elif colour == 'red':
        colour = '91m'
    elif colour == 'green':
        colour = '92m'
    elif colour == 'yellow':
        colour = '93m'
    elif colour == 'blue':
        colour = '94m'
    else: return text

    return f'\033[{colour}{text}\033[0m'

def parse_args():
    '''Parse command line arguments'''

    parser = argparse.ArgumentParser()

    parser.add_argument('--username',
                        required=True,
                        help="Specify the username")

    parser.add_argument('--password',
                        required=False,
                        help="Specify the password")

    parser.add_argument('--environment',
                        required=True,
                        help="Specify the Environment")

    parser.add_argument('--action',
                        required=True,
                        help="Specify the task you want to perform")

    args = parser.parse_args()

    if not args.password:

        args.password = getpass.getpass(("Please enter the Password for {}: "
                                         .format(args.username)))
    return args

def main():
    '''Main'''

    args = parse_args()

    tuono = tuono_api.Tuono(args.username, args.password)

    environments = tuono.get_environments()

    for environment in environments:
        if environment['name'] == args.environment:
            environment_id = environment['_id']
            print(f"\nRunning a(n) {colours(args.action, 'bold')}")
            print(f"environment: {colours(args.environment, 'blue')}")
            print(f"id: {colours(environment_id, 'blue')}")
    try:
        environment_id
    except NameError:
        print(f"\n{colours(args.environment, 'red')} not found in Environment list\n")
        sys.exit(1)

    payload = {"action"        : args.action,
               "environment_id": environment_id}

    job = tuono.complete_action(payload)

    job_id = job['_id']

    print(f"\nStarting Job {colours(job_id, 'green')}\n")

    job_status = tuono.get_job(job_id)

    while job_status['status'] != "succeeded":
        if job_status['status'] == "failed":
            print(f"{colours('The job Failed. Please retry', 'red')}\n")
            sys.exit(1)
        else:
            print((f'{datetime.datetime.now().strftime("%H:%M:%S")} '
                   f'{colours(job_status["status"].capitalize(), "yellow")}'))
            time.sleep(10)
            job_status = tuono.get_job(job_id)

    print(f"\n{colours('Success!', 'green')}\n")
    print(json.dumps(job_status['job_details']['summary'], indent=2, sort_keys=True))

if __name__ == '__main__':

    main()
