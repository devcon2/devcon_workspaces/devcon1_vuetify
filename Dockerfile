#  Copyright (c) 2020 Indigon, Inc
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

FROM ubuntu:18.04

# Install base development tools need for development.
RUN apt-get update
RUN apt-get install -y sudo
RUN apt-get install -y vim
RUN apt-get install -y git
RUN sudo apt-get install -y curl


# **** Setup SSH server ****
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
RUN sudo apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:devcon1' | chpasswd
RUN sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd
RUN RUNLEVEL=1 dpkg-reconfigure openssh-server
RUN update-rc.d ssh defaults

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# **** Setup Specific Language Devlopement dependencies ****
# * Node.JS and Vuetify specific development.
# [TODO] Parameterize this to make this a template that can be reused for other Languages and frameworks.
RUN curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
RUN sudo apt-get install -y nodejs
RUN npm install -g @vue/cli

# **** Setup Username ****
# NOTE: must be at the bottom so the above commands are run as Root
# Set "set to system", create the home directory, shell=/bin/bash, primary group=Root, additional groups, sudo, userid=1001, password=devcon
# Set ownership to user on the home directory.
# Add all users to the sudoer's file to have access to the system like root.
RUN useradd -rm -d /home/devcon -s /bin/bash -g root -G sudo -u 1001 -p "$(openssl passwd -1 devcon)" devcon && \
    chown -R devcon /home/devcon && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER devcon
WORKDIR /home/devcon
RUN mkdir /home/devcon/git-repo

# Satart the SSHD server through "service"
# then start a Bash session to connect to when testing directly using "docker exec -it..."
EXPOSE 22
EXPOSE 80
EXPOSE 443
ENTRYPOINT sudo /etc/init.d/ssh start && /bin/bash
